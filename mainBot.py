import time
import sys
import importlib
import logging
import types
import json
import os

import irc
import commands
from events import Event
from database import Database

class ShatnerBot:

    ircConnection = irc.IrcConnect()
    ircPrefix = ""

    username = "shatnerbot"
    channels = []

    sysCommands = {}
    chatCommands = {}
    simpleCommands = {}

    admins = []
    db = None

    def __init__(self):
        self.ircConnection.connect()
        self.db = Database()

    def reload(self):
        self.loadAdmins()
        importlib.reload(commands.sysCommands)
        self.loadSysCommands()
        importlib.reload(commands.chatCommands)
        self.loadChatCommands()
        self.loadSimpleCommands()

    def checkMessages(self):
        return self.ircConnection.checkMessages()

    def loadAdmins(self):
        adminFile = open("admins.txt", 'r')
        #filter out extraneous newlines
        self.admins = adminFile.read().split("\n")
        for admin in self.admins:
            if not admin:
                self.admins.remove(admin)
        adminFile.close()

    ##Loads available commands from the irc.commands package
    ##self.commands[commandName] is now a reference to the function, and can be called
    def loadSysCommands(self):
        allCommands = dir(commands.sysCommands)
        foundCommands = []
        for command in allCommands:
            if command[:2] != '__':
                foundCommands.append(command)
        for command in foundCommands:
            func = getattr(commands.sysCommands, command)
            if type(func) == types.FunctionType:
                self.sysCommands[command] = func

    def loadChatCommands(self):
        self.chatCommands = commands.chatCommands.COMMANDS
        # allCommands = dir(commands.chatCommands)
        # foundCommands = []
        # for command in allCommands:
        #     if command[:2] != '__':
        #         foundCommands.append(command)
        # for command in foundCommands:
        #     func = getattr(commands.chatCommands, command)
        #     # if type(func) == types.FunctionType:
        #     #     self.chatCommands[command] = func
        #     if func.__name__ in commands.chatCommands.COMMANDS:
        #         self.chatcommands[command] = func

    def loadSimpleCommands(self):
        with open("simpleCommands.json", 'r') as infile:
            self.simpleCommands = json.load(infile)
            # command = []
            # for line in file:
            #     if line != "\n":
            #         command.append(line)
            #     try:
            #         self.simpleCommands[command[0].strip("\n")] = command[1].strip("\n")
            #         command = []
            #     except:
            #         pass

    def callSysCommand(self, event):
        command = event.command
        commandParameters = event.params

        if command not in self.sysCommands:
            return False

        else:
            self.sysCommands[command](self, event)

    def callChatCommand(self, event):
        if event.command not in self.chatCommands:
            return False
        else:
            try:
                self.chatCommands[event.command](self, event)
            except Exception:
                logging.exception("Someone did a bad")
                self.sendMessage(event.channel, "You did a bad")
                # raise
            except:
                raise

    def isChatCommand(self, command):
        if command in self.chatCommands:
            return True
        else:
            return False

    def isAdmin(self, name):
        if name in self.admins:
            return True
        else:
            return False

    def isSimpleCommand(self, command):
        if command in self.simpleCommands:
            return True
        else:
            return False

    def callSimpleCommand(self, event):
        self.sendMessage(event.channel, self.simpleCommands[event.command])

    def isFromAdmin(self, event):
        return self.isAdmin(event.username)

    def setIRCPrefix(self, prefix):
        self.ircPrefix = prefix

    def isCommand(self, command):
        if command in self.simpleCommands:
            return True
        elif command in self.sysCommands:
            return True
        elif command in self.chatCommands:
            return True
        else:
            return False

    #not necessarily the best place for it, but I'm fart too tired of typing it long form
    def sendMessage(self, channel, message):
        self.callSysCommand(Event(command = "sendChatMessage", username = "shatnerbot", channel = channel, params = [message]))
    # 
    # def restart(self):
    #     # os.execv(os.path.join(os.getcwd())+__file__, sys.argv)
    #     # os.execv(__file__, sys.argv)

def chatFilter(bot, message):
    print (message)

    if message["command"] == "JOIN":
        if not bot.db.isUser(message["prefix"][:message["prefix"].find("!")]):
            bot.db.addUser(message["prefix"][:message["prefix"].find("!")])
            print("user added: {}".format(message["prefix"][:message["prefix"].find("!")]))

    if message["command"] != "PRIVMSG": #abstract this more later, so I don't need to check for IRC related things
        return False
    else:
        #I don't remember what this does
        try:
            chatMessage = message["parameters"][1]
        except:
            sys.exit("Something went terribly wrong")
        else:
            if len(chatMessage) > 1 and chatMessage[0] == "!":
                command = list(filter(None, chatMessage[1:].split(" ")))[0].lower() #I want a lower here, makes things easier
                params = list(filter(None, chatMessage[1:].split(" ")))[1:]
                username = message["prefix"][:message["prefix"].find("!")] #there should always be a ! in a prefix
                channel = message["parameters"][0]
                event = Event(command = command, username = username, channel = channel, params = params)
                logging.info("{} called {}".format(username, command))

                if bot.isSimpleCommand(command):
                    bot.callSimpleCommand(event)

                elif bot.isChatCommand(command):
                    bot.callChatCommand(event)
                else:
                    #turn this off later, just for debugging
                    bot.sendMessage(event.channel, event.command + " is not a valid command")

def main():
    # logging.basicConfig(filename="shatnerBot.log", level=logging.INFO, format='%(asctime)s %(message)s')
    logging.basicConfig(filename="shatnerBot.log", level=logging.INFO, format='%(asctime)s %(message)s')

    bot = ShatnerBot()
    bot.loadSysCommands()
    bot.loadChatCommands()
    bot.loadAdmins()
    bot.loadSimpleCommands()
    bot.callSysCommand(Event(command="joinChannel", username="shatnerbot", channel = None,  params=["#shatnerbot"]))
    while True:
        messages = bot.checkMessages()
        try:
            time.sleep(0.5)
        except KeyboardInterrupt:
            logging.warning("Keyboard interrupt")
            sys.exit()
        if not messages:
            continue
        for message in messages:
            chatFilter(bot, message)

            #this is hacky, but I think it still works.
            if message["command"] == "JOIN":
                bot.setIRCPrefix(":" + message["prefix"])

if __name__ == '__main__':
    main()
