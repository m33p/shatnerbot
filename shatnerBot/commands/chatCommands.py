import sys
import datetime
import logging
import random
import string

import pytz

import pywapi #python weather API

import irc
import arctosisUtils
import weather as Weather
from events import Event
from commands.decorators import *

#Import custom functions or modules here:
from customModules import *

##Always have commands lowercase, makes it easier for everything
#I know it feels wrong, but just do it

##TODO: Set a quit message before stopping
@adminOnly
@command
def quit(bot, event):
    """It just makes the bot stop. That's it."""
    bot.sendMessage(event.channel, "Bai!")
    sys.exit("Stopped from chat")

# @adminOnly
# @command
# def restart(bot, event):
#     bot.restart()

@adminOnly
@command
def joinchannel(bot, event):
    if event.params[0][0] != "#":
        event.params[0] = "#" + event.params[0]
    bot.callSysCommand(Event(command="joinChannel", username = bot.username, channel = None, params = [event.params[0]] ))

@adminOnly
@command
def partchannel(bot, event):
    if event.params[0][0] != "#":
        event.params[0] = "#" + event.params[0]
    bot.callSysCommand(Event(command="partChannel", username = bot.username, channel = None, params = [event.params[0]] ))


@adminOnly
@command
def addadmin(bot, event):
    if not event.params:
        bot.sendMessage("No user specified")
        return
    else:
        adminName = event.params[0]
        adminFile = open("admins.txt", 'r')
        admins = adminFile.read().split("\n")
        admins = list(filter(None, admins))
        if adminName in admins:
            bot.sendMessage(event.channel, "{} is already an admin.".format(adminName))
            return
        adminFile.close()
        adminFile = open("admins.txt", 'w')
        admins.append(adminName)
        for name in admins:
            adminFile.write(name + "\n")
        adminFile.close()
        bot.loadAdmins()
        if bot.isAdmin(adminName):
            bot.sendMessage(event.channel, "{} is now an admin".format(adminName))
        else:
            bot.sendMessage(event.channel, "{} not successfully addded.".format(adminName))

@adminOnly
@command
def listadmins(bot, event):
    admins = ""
    for admin in bot.admins:
        admins += admin + ", "
    #cheap remove final comma
    admins = admins[:-2]
    bot.sendMessage(event.channel, admins)

@adminOnly
@command
def reload(bot, event):
    """This reloads the bot"""
    adminNum = len(bot.admins)
    sysCommandNum = len(bot.sysCommands)
    chatCommandNum = len(bot.chatCommands)
    simpleCommandNum = len(bot.simpleCommands)

    bot.reload()

    newAdminNum = len(bot.admins)
    newSysCommandNum = len(bot.sysCommands)
    newChatCommandNum = len(bot.chatCommands)
    newSimpleCommandNum = len(bot.simpleCommands)

    adminDiff = arctosisUtils.readableDiff(adminNum, newAdminNum)
    sysCommandDiff = arctosisUtils.readableDiff(sysCommandNum, newSysCommandNum)
    chatCommandDiff = arctosisUtils.readableDiff(chatCommandNum, newChatCommandNum)
    simpleCommandDiff = arctosisUtils.readableDiff(simpleCommandNum, newSimpleCommandNum)

    changeMessage = "Bot reloaded. Admins: {0}.  System Commands: {1}.  Chat Commands: {2}, Simple Commands: {3}.".format(adminDiff, sysCommandDiff, chatCommandDiff, simpleCommandDiff)

    bot.sendMessage(event.channel, changeMessage)

@command
def currdate(bot, event):
    bot.sendMessage(event.channel, datetime.datetime.now().strftime("%B %d, %Y"))


##TODO This needs to make more sense
@command
def help(bot, event):
    """This command was deprecated, please listen to it

    This is a command that currently does nothing, but here is
    some more advanced help on it anyway for testing
    """
    #start working on docs for commands, and put 'em here
    #also allow searching for partial commands
    bot.sendMessage(event.channel, "try !commandName help instead, or !getcommands for a list")

@command
def getcommands(bot, event):
    bot.sendMessage(event.channel, str(list(bot.chatCommands.keys())))

## TODO move simple commands to their own module
# @adminOnly
@command
def newsimplecommand(bot, event):
    print (event.params)
    for char in event.params[0]:
        if char in string.punctuation:
            bot.sendMessage(event.channel, "No punctuation allowed in command names")
            return
    if len(event.params) <= 1:
        bot.sendMessage(event.channel, "Please specify command and reply")
    else:
        bot.callSysCommand(Event(command="addSimpleCommand", username=bot.username, channel = event.channel, params = event.params))

@command
def getsimplecommands(bot, event):
    bot.sendMessage(event.channel, str(bot.simpleCommands.keys()))

@command
def listusers(bot, event):
    users = []
    for user in bot.db.getUsers():
        users.append(user["nickname"])
    bot.sendMessage(event.channel, str(users))


@command
def printcommands(bot, event):
    bot.sendMessage(event.channel, str(COMMANDS))
