#I don't fully understand how the python importing system works

COMMANDS = {}

#adminOnly decorator that checks to see if the user that sent the command is admin
def adminOnly(func):
    """A decorator that checks if the user is admin, allowing for admin-only commands"""
    def wrapper(*args, **kwargs):
        if args[0].isFromAdmin(args[1]):
            func(*args, **kwargs)
        else:
            args[0].sendMessage(args[1].channel, "You aren't Admin")
    return wrapper

def command(func):
    """A decorator that displays docstrings if the first param is help"""
    def wrapper(*args, **kwargs):
        if len(args[1].params) > 0 and args[1].params[0].lower() == "help":
            doc = func.__doc__
            helpMessage = ""
            if not doc:
                helpMessage = "Help is not available"
            else:
                helpMessage = "Help for !{}: {}".format(func.__name__, doc)

            args[0].sendMessage(args[1].channel, helpMessage)
        else:
            func(*args, **kwargs)
    COMMANDS[func.__name__] = wrapper
    return wrapper
