from commands.decorators import *

from pytz import timezone
from datetime import datetime
import pytz
from pytz.exceptions import UnknownTimeZoneError

@command
def get_time(bot, event):
    channel = event.channel
    utc = pytz.utc
    utc_dt = utc.localize(datetime.now()).replace(tzinfo=utc)
    fmt = '%m/%d %H:%M'
    
    try:
        time_zone_input = event.params[0]
        if len(time_zone_input) < 1: #makes sure there is a value for <search string>
            bot.sendMessage(channel, 'Syntax: .time <GMT offset or timzone>')
            return
    except IndexError:
        bot.sendMessage(channel, utc_dt.strftime(fmt))# user didn't specify a time, return GMT
        return
    
    try:
        offset = int(time_zone_input)
    except ValueError:
        offset = None
        time_zone_input = time_zone_input.strip()
    
    if offset:
        try:
            if offset < 0:
                user_tz = timezone('Etc/GMT+{0}'.format(offset * -1))#for some reason the GMT+4 == America/Eastern, and GMT-4 is over in Asia
            elif offset > 0:
                user_tz = timezone('Etc/GMT{0}'.format(offset * -1))
            elif offset == 0:
                user_tz = utc
        except UnknownTimeZoneError:
            bot.sendMessage(channel, 'Invalid GMT offset')
            return
    else:
        try:
            user_tz = timezone(time_zone_input)
        except UnknownTimeZoneError:
            bot.sendMessage(channel, 'Invalid Timezone')
            return
            
    user_dt = user_tz.normalize(utc_dt.astimezone(user_tz))
    bot.sendMessage(channel, user_dt.strftime(fmt))