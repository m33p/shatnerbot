#!/usr/bin/python

decorated = []

def notCallable(func):
    decorated.append(func.__name__)
    return func


def command(func):
    def wrapper(*args, **kwargs):
        if args[0] == "help":
            print (func.__doc__)
        return func(*args, **kwargs)
    return wrapper

@notCallable
def testingfunction(paramString):
    """This is a function that does things"""
    # bot.sendMessage(event.channel, "Just testing")

print(decorated)
