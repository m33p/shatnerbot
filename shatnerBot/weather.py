import string

import dataset
import pywapi

#i need to change this from being weather specific, to being a general database wrapper
#so I can just use simple syntax to check users for params in the database

#also be careful, because you can't easily remove columns from these databases right now

def verifyZip(zipcode):
    if len(zipcode) == 5:
        for char in zipcode:
            if char not in string.digits:
                return False
        return True
    else:
        return False

def returnWeather(zipcode):
    # zipcode = event.params[0]
    if verifyZip(zipcode):
        try:
            result = pywapi.get_weather_from_yahoo(zipcode)
            currWeather = "It is " + result['condition']['text'].lower() + " and " + str(((float(result["condition"]['temp'])*(9.0/5.0))+32)) + "F now in {}.\n".format(zipcode)
            return currWeather
        except:
            return "Weather is not available"
    else:
        return "Zipcode is invalid"
    # bot.callSysCommand("sendChatMessage", currWeather)
