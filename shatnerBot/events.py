#Something Something events are here or something

#currently just basically a struct, but can be improved as necessary
#currently it makes things much more straightforward when writing chatcommands
#because now you can just have a command take (bot, event)
#and you don't have to worry about wrong number of arguments

class Event():
    command = ""
    username = ""
    channel = ""
    params = []

    def __init__(self, command, username, channel, params):
        self.command = command
        self.username = username
        self.channel = channel
        self.params = params

    def __str__(self):
        return "Command: {}, Username: {}, Channel: {},  params: {}".format(self.command, self.username, self.channel, str(self.params))
