from commands.decorators import *

@command
def hello(bot, event):
    bot.sendMessage(event.channel, "Hello {}".format(event.username))
