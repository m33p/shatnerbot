from commands.decorators import *

import random
import string

import lxml.html


@command
def randomimgur(bot, event):
    url = genUrl("http://i.imgur.com/")
    title = isValidUrl(url)
    while not title:
        url = genUrl("http://i.imgur.com/")
        title = isValidUrl(url)
    bot.sendMessage(event.channel, "{} {}".format(title, url))

@command
def randomreddit(bot, event):
    url = genUrl("http://reddit.com/")
    title = isValidUrl(url)
    while not title:
        url = genUrl("http://reddit.com/")
        title = isValidUrl(url)
    bot.sendMessage(event.channel, "{} {}".format(title, url))

def isValidUrl(url):
    try:
        page = lxml.html.parse(url)
        title = page.find(".//title").text
        print(title.strip())
        return title.strip()
    except:
        return False

def genUrl(root):

    rand = ''.join(random.choice(string.ascii_letters+string.digits) for i in range(random.choice([5,7])))
    url = root + rand
    return url
