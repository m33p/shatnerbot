import json

import irc

##Starting with underscores so it isn't callable from the Bot class
def __makeDict(prefix = "shatnerbot!shatnerbot@shatnerbot.tmi.twitch.tv", command = "", parameters = [], trail = ""):
    message = {}
    message["prefix"] = prefix
    message["command"] = command
    message["parameters"] = parameters
    message["trail"] = trail
    return message

def joinChannel(bot, event):
    channelName = event.params[0]
    message = __makeDict( command = "JOIN", parameters = [channelName])
    print("Joining Channel", channelName)
    bot.ircConnection.send(irc.encode(message))
    bot.channels.append(channelName)
    print("Channel Joined")

def partChannel(bot, event):
    channelName = event.params[0]
    message = __makeDict(command = "PART", parameters = [channelName])
    print("Parting Channel", channelName)
    bot.ircConnection.send(irc.encode(message))
    bot.channels.remove(channelName)
    print("Channel Parted")

def sendChatMessage(bot, event):
    channel = event.channel
    sendMessage = event.params[0]
    message = __makeDict(command = "PRIVMSG", parameters = [channel], trail = sendMessage)
    bot.ircConnection.send(irc.encode(message))

def addSimpleCommand(bot, event):
    if bot.isCommand(event.params[0]):
        bot.sendMessage(event.channel, "Command already exists")
        return
    else:
        response = " ".join(event.params[1:])
        command = event.params[0].lower()
        currentCommands = bot.simpleCommands
        currentCommands[command] = response
        with open("simpleCommands.json",'w') as outfile:
            json.dump(currentCommands, outfile)
        bot.loadSimpleCommands()
        bot.sendMessage(event.channel, "Command Added")

# def getUsers(bot, channel):
#     message = __makeDict(command = "NAMES", parameters = [channel])
#     bot.ircConnection.send(irc.encode(message))
