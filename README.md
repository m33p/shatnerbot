# This. Is Shatner. Bot. #

This is a basic IRC/Twitch bot.
There's not a lot to say about it, really.
The main philosophy is making new commands as modular as possible.

## Getting it Running ##
It's pretty simple to get running, just clone, make a virtualenv named env, and pip install the requirements file.
If you can't get pywapi, I've included the latest build in the api folder, just extract and run setup.py with the env's python bin.

## Adding Custom Modules ##
Just make a python module and put it in the customModules directory
Then, add an import to the __init__ and make sure that your files have

```
#!python
from commands.decorators import *

```

at the top.

## Requirements ##

Python >= 3.3
Most of the requirements are in the pip freeze generated requirements.txt, and can be installed with pip.
In some circumstances, pip doesn't want to install pywapi, so it's currently included in the libs directory.

The script is currently hosted [here](https://code.google.com/p/python-weather-api/) on Google Code, but since it's closing soon, I don't know how long access will remain. Another source appears to be [launchpad](https://launchpad.net/python-weather-api).
