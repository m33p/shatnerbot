Moved to https://gitlab.com/m33p/shatnerbot

# This. Is Shatner. Bot. #

This is a basic IRC/Twitch bot.
There's not a lot to say about it, really.
The main philosophy is making new commands as modular as possible.

## Getting it Running ##
It's pretty simple to get running, just clone, make a virtualenv named env, and pip install the requirements file
if you can't get pywapi, I've included the latest build in the api folder, just extract and run setup.py with the env's python bin

## Adding Custom Modules ##
Just make a python module and put it in the customModules directory
Then, add an import to the __init__ and make sure that your files have

```
#!python
from commands.decorators import *

```

at the top