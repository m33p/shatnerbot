#Outdated
"""
def parseMessage(data):
    data = data.decode()
    pos = data.find("PRIVMSG")
    if pos == -1:
        return False
    else:
        userEnd = data.find("!")
        user = data[1:userEnd]
        messageStart = data.find(" :")
        message = data[messageStart+2:]
        return "{}: {}".format(user, message)
        # return data[pos:]

#Logic From http://calebdelnay.com/blog/2010/11/parsing-the-irc-message-format-as-a-client
#not quite pythonic at the top, can be cleaned later
def decode(message):
    #ensure message is a string
    if type(message) == type(b''):
        message = message.decode()

    splitMessage = {}
    prefixEnd = -1
    trailingStart = len(message)
    trailing = ""
    prefix = ""
    command = ""
    parameters = []

    #remove \r\n from end of message, if it exists
    if message[-1] == "\n":
        message = message[:-1]
    if message[-1] == "\r":
        message = message[:-1]

    #get prefix
    if message[0] == ":":
        prefixEnd = message.find(" ")
        prefix = message[1:prefixEnd]

    trailingStart = message.find(" :")
    if trailingStart >= 0:
        trailing = message[trailingStart + 2:]
    else:
        trailingStart = len(message)

    parameters = message[prefixEnd + 1:trailingStart].split(" ")

    command = parameters[0]
    parameters = parameters[1:]
    # if len(parameters) > 1:
    #     parameters = parameters[1:]

    if trailing:
        parameters.append(trailing)

    splitMessage["prefix"] = prefix
    splitMessage["command"] = command
    splitMessage["parameters"] = parameters
    return splitMessage

def encode(splitMessage):
    encodedMessage = ""
    if splitMessage["prefix"]:
        encodedMessage += ":" + splitMessage["prefix"]
    if encodedMessage:
        encodedMessage += " "
    encodedMessage += splitMessage["command"]
    for parameter in splitMessage["parameters"]:
        encodedMessage += " " + parameter

    encodedMessage += "\r\n"
    return encodedMessage.encode()


def removeCarriage(message):
    return message[:-2]

def process(rawMessage):
    returnMessage = removeCarriage(rawMessage)
"""
