import dataset

class Database():
    db = None
    tables = {}

    def __init__(self):
        self.db = dataset.connect("sqlite:///shatnerbot.db")
        self.tables["users"] = self.db["users"]

    def addUser(self, user):
        if not self.isUser(user):
            self.tables["users"].insert(dict(nickname = user))
            return True
        else:
            return False

    def isUser(self, user):
        if self.tables["users"].find_one(nickname=user):
            return True
        else:
            return False

    def getUsers(self):
        return self.tables["users"].all()

    #check for valid columns before doing this?
    #Expects username, columnName, info
    def updateUser(self, user, column, info):
        if self.isUser(user):
            foundUser = self.tables["users"].find_one(nickname = user)
            foundUser[column] = info
            self.tables["users"].update(foundUser, ["nickname"])
            return True
        else:
            return False

    def getUser(self, user):
        if self.isUser(user):
            return self.tables["users"].find_one(nickname = user)
        else:
            return False
