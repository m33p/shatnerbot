from commands.decorators import *

import string

import dataset
import pywapi

#i need to change this from being weather specific, to being a general database wrapper
#so I can just use simple syntax to check users for params in the database

#also be careful, because you can't easily remove columns from these databases right now

def verifyZip(zipcode):
    if len(zipcode) == 5:
        for char in zipcode:
            if char not in string.digits:
                return False
        return True
    else:
        return False

def returnWeather(zipcode):
    # zipcode = event.params[0]
    if verifyZip(zipcode):
        try:
            result = pywapi.get_weather_from_yahoo(zipcode)
            currWeather = "It is " + result['condition']['text'].lower() + " and " + str(((float(result["condition"]['temp'])*(9.0/5.0))+32)) + "F now in {}.\n".format(zipcode)
            return currWeather
        except:
            return "Weather is not available"
    else:
        return "Zipcode is invalid"
    # bot.callSysCommand("sendChatMessage", currWeather)


#turns out this is more complicted than it looked.
#I need to use both zipcodes and location ids, and have a search for locations so users can get their
#location id

#I'll stick with zipcodes for now though
@command
def weather(bot, event):
    if event.params: #assume they want a specified weather
        currWeather = returnWeather(event.params[0])
        bot.sendMessage(event.channel, currWeather)
    elif bot.db.isUser(event.username):
        user = bot.db.getUser(event.username)
        if user["zip"]:
            currWeather = returnWeather(user["zip"])
            bot.sendMessage(event.channel, currWeather)
    else:
        bot.sendMessage(event.channel, "You did not specify a location, and do not have one saved.") #TODO Make this error better

@command
def setzip(bot, event):
    if bot.db.updateUser(event.username, "zip", event.params[0]):
        bot.sendMessage(event.channel, "Zip successfully added")
    else:
        bot.sendMessage(event.channel, "Zip not successfully added")

@command
def getzip(bot, event):
    user = bot.db.getUser(event.username)

    if user["zip"]:
        bot.sendMessage(event.channel, user["zip"])
