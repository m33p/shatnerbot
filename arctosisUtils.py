#A file full of useful functions and stuff that I want to keep around

##currently just works on ints
def readableDiff(prev, curr):
    diff = curr - prev
    if diff > 0:
        return "{}  added".format(diff)
    elif diff < 0:
        return "{} removed".format(abs(diff))
    else:
        return "0 changed"
