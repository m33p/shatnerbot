import socket
import sys
import time
import logging
import configparser

# from .commands import *

class IrcConnect:
    sock = None

    config = configparser.ConfigParser()

    def __init__(self):
        try:
            self.config.read("config.ini")
            self.host = self.config["server"]["host"]
            self.port = int(self.config["server"]["port"])

            self.username = self.nickname = self.config["user"]["username"]
            self.password = self.config["user"]["password"]
            self.twitchOauth = self.config["user"]["twitchOauth"]

        except:
            ##TODO Log error, make better exit message
            sys.exit("Cannot Load config")

    def connect(self):
        try:
            self.sock = socket.socket()
            self.sock.settimeout(1)
            self.sock.connect((self.host, self.port))
            #final string needs a space if on localhost, not sure why
            #shatner bot instead of shatnerbot

            self.send("USER {0} {1} bla : {2}\r\n".format(self.username, self.host, self.username).encode())

            ##TODO There will never be a self.twitchOauth anymore, so this is unnecessary
            if self.twitchOauth:
                self.password = self.twitchOauth
            if self.password:
                self.send("PASS {}\r\n".format(self.password).encode())

                """"""
            self.send("NICK {}\r\n".format(self.nickname).encode())
        except:
            logging.warning("Connection Failed")
            sys.exit("Failed to connect")

        else:
            messages = []
            connected = False
            #please fix this later, this should go by logic, not by count
            for x in range(0,25):
                if connected:
                    break
                time.sleep(5) #give the messages a chance to arrive. Need to make this adapt somehow
                messages = self.checkMessages()
                if not messages:
                    break
                for message in messages:
                    if message["command"] == "004":
                        connected = True
                        logging.info("Connected to {}".format(self.host))
                        print("Connected")
                        break
                if not messages:
                    sys.exit("Failed to recieve reply")

            if not connected:
                sys.exit("Failed to connect")


    ## Returns list of messages
    def checkMessages(self, amount = 1024):
        data = None
        try:
            data = self.sock.recv(amount)
        except:
            return False;
        else:
            # print("data recieved: ", data)
            data = data.decode()

            data = data.split("\r\n")
            messages = []
            for line in data[:-1]:
                messages.append(decode(line))
            # decodedData = decode(data)
            #As I learn more commands for IRC messages, my "driver" will probably have another section for this
            for message in messages:
                if message["command"] == "PING":
                    # print("ping")
                    message["command"] = "PONG"
                    encodedData = encode(message)
                    self.send(encodedData)
                    messages.remove(message)
                    # print("pong")
                    continue
            return messages

    #Only accepts a byte string from tools.encode()
    def send(self, message):
        self.sock.send(message)

    def sendPrivMessage(self, message):
        sendMessage = "PRIVMSG "+ self.channel +" :{}\r\n".format(message)
        self.sock.send(sendMessage.encode())

    def joinChannel(self, channel):
        self.sock.send("JOIN {}\r\n".format(channel).encode())


#Logic From http://calebdelnay.com/blog/2010/11/parsing-the-irc-message-format-as-a-client
#not quite pythonic at the top, can be cleaned later
def decode(message):
    #ensure message is a string
    if type(message) == type(b''):
        message = message.decode()

    splitMessage = {}
    prefixEnd = -1
    trailingStart = len(message)
    trailing = ""
    prefix = ""
    command = ""
    parameters = []

    #remove \r\n from end of message, if it exists
    if message[-1] == "\n":
        message = message[:-1]
    if message[-1] == "\r":
        message = message[:-1]

    #get prefix
    if message[0] == ":":
        prefixEnd = message.find(" ")
        prefix = message[1:prefixEnd]

    trailingStart = message.find(" :")
    if trailingStart >= 0:
        trailing = message[trailingStart + 2:]
    else:
        trailingStart = len(message)

    parameters = message[prefixEnd + 1:trailingStart].split(" ")

    command = parameters[0]
    parameters = parameters[1:]
    # if len(parameters) > 1:
    #     parameters = parameters[1:]

    if trailing:
        parameters.append(trailing)

    splitMessage["prefix"] = prefix
    splitMessage["command"] = command
    splitMessage["parameters"] = parameters
    return splitMessage

##Expects dict
def encode(splitMessage):
    encodedMessage = ""
    if "prefix" in splitMessage:
        encodedMessage += ":" + splitMessage["prefix"]
    if encodedMessage:
        encodedMessage += " "
    encodedMessage += splitMessage["command"]
    for parameter in splitMessage["parameters"]:
        encodedMessage += " " + parameter
    if "trail" in splitMessage:
        encodedMessage += " :" + splitMessage["trail"]
    encodedMessage += "\r\n"
    # print ("Data encoded: ", encodedMessage.encode())
    return encodedMessage.encode()

##Take raw parameters, output dict
def messageFormat(prefix = None, command = None, parameters = None):
    messageDict = {}
